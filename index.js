console.log("hello world")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
// + - * / % 


//Addition Assignment Operator(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

//Subtraction/Multiplication/division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);


//Multiple Operators and Parenthesis
/*
 -when multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
 - The operations were done in the ff order:
    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    3. 1 + 2 = 3
    4. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


//Increment and Decrement Operator
//Operators that add or subract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation.
//The value of "z" is added by a value of 1 before returning the value and storing it int the variable "increment".
++z;
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

//post-fix incrementation
//The value of "z" is returned and stored in the variable "increment" then the value of 'z' is increased by one
z++;
console.log(z); //3 - The value of z was added with 1
console.log(z++); //3 - the previous value of the variable is returned.
console.log(z); //4 - a new value is no returned.

//pre-fix vs post-fix incrementation
console.log(z++); //4
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately.


//pre-fix and post-fix decrementation
console.log(--z); //5 - with pre-fix decrementation the result of subtraction by 1 is returned immediately

console.log(z--); //5 - with post-fix decrementation the result of subtraction by q is not immediately returned, instead the previous value is returned first
console.log(z); //4



//Type Coercion
 //is the automatic or implicit conversion of values from one data type to another
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);

//Adding/Contatenating a string and a number will result to a string


let numC = 16;
let numD = 14;


let nonCoercion = numC + numD;
console.log(nonCoercion)
console.log(typeof nonCoercion)

//the boolean "true" is also associated with the value of 1
let numE = true + 1;
console.log(numE); //
console.log(typeof numE); //number

//The boolean "false" is also associated with the value of 0
let numF = false + 1;
console.log(numF);



//Comparison Operators
let juan = 'juan';

// (==) Equality Operator
//checks whether the operands are equal/have the same content
//Attempts to CONVERT AND COMPARE operands of different data types
//Returns a boolean value

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); //false, case sensitive
console.log('juan' == juan); //true

//(!=) Inequality Operator
//checks whether the operands are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false);
console.log('juan' != 'JUAN');
console.log('juan' != juan);

//(===) Strictly Equality Operator
console.log('Strictly Equality Operator')
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false = not the same data type
console.log(0 === false); //false = different data typ
console.log('juan' === 'JUAN'); //false, case sensitive
console.log('juan' === juan); //true

//(!==) Strict Inequality Operator
console.log('Strictly Inequality Operator')
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN');
console.log('juan' !== juan);


//Relational Comparison Operators
//check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("Greater than")
//Greater than (>)
console.log(x > y); //false
console.log(w > y); //true

console.log("Less than")
//Less than (<)
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion 
console.log(numString < 6000); //true - forced coercion to change string to number
console.log(numString < "Jose"); //true "5500" < "Jose"

//Greater Than Or Equal To
console.log(w >= 8000); //true

//Lesst Than Or Equal To
console.log(x <= y); //true
console.log(y <= y); //true


//Logical Operators

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical AND Operators")
//Logical AND Operator (&& - Double Ampersand)
//Returns true if ALL operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true


let requiredLevel = 95;
let requiredAge = 18;


let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); //true


let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;


let registration1 = userName.length > 8 && userAge >= requiredAge;
//.length is a property of strings which determine the number of characters in the string.
console.log(registration1); //false - 

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true



console.log("Logical OR Operator");
//OR Operator (|| - Double Pipe)
//returns true if atleast ONE of the operands are true


let userLevel = 100;
let userLevel2 = 65;


let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); //true


let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2); //false



console.log("Not Operator")
//NOT Operator (!)
//turns a boolean into the opposite value


let guildAdmin = !isAdmin || userLevel2 >=requiredLevel;
console.log(guildAdmin); //true


console.log(!isRegistered); //false
console.log(!isLegalAge); 

let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite); //true - isAdmin original value = false
console.log(opposite2); //false - isLegalAge orignal value = true


//if, else if, and else statement

//IF statement
//if statement will run a code block if the condition specified is true or results to true

// if(true){
//  alert("We just run an if condition");
// }


let numG = -1;

if(numG < 0) {
    console.log('Hello');
}


let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
    console.log("Welcome to Game Online!");
}
if(userLevel3 >= requiredLevel) {
    console.log("You are qualified to join the guild!")
}

if(userName3.length >= 10 && isRegistered && isAdmin) {
    console.log("Thank you for joining the Admin!")
} else {
    console.log("You are not ready to be an Admin")
}

//ELSE statement
//The "else" statement executes a block of codes if all other conditions are false

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
    console.log("Thank you for joining the Noobies Guild!");
} else {
    console.log("You are too strong to be a noob. :( ")
}


//else if statement
//else if executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
    console.log("Thank you for joining the noobies guild.")
} else if(userLevel > 25) {
    console.log("You too strong to be a noob.")
} else if(userAge3 < requiredAge) {
    console.log("You're too young to join the guild.")
} else {
    console.log("better luck next time.")
}


//if, else if and else statement with functions

function addNum(num1, num2) {
    //check if the numbers being passed are number types.
    //typeof keyword returns a string which tells the type of data that follows it
    if(typeof num1 === "number" && typeof num2 === "number") {
        console.log("Run only if both arguments passed are number types");
        console.log(num1 + num2);
    } else {
        console.log("One or both of the arguments are not numbers");
    }
}

addNum(5, '2');

let numsample = true;
console.log(typeof numsample)


//create log in function

function login(username, password) {
    //check if the argument passed are strings
    if(typeof username === "string" && typeof password === "string"){
        console.log("Both Arguments are strings.")

        /*
            nested if-else
                will run if the parent if statement is able to agree to accomplish its condition

            Mini-Activity:

            add another condition to our nested if statement:
                -check if username is atleast 8 characters long
                -check if the password is atleast 8 characters long.
                -show an alert which says "Thank you for logging in"
            add an else statement which will run if both condition were not met:
                -show an alert which says "Credentials too short."


            Stretch Goals: 

            add an else if statement that if username is less than 8 characters
                -show an alert "Username is too short"
            add an else if statement if the password is less than 8 characters
                -show an alert "password too short"


            screenshot/video the alert messages output in our gc.

        */

        if(username.length >= 8 && password.length >= 8) {
            console.log("Thank you for logging in")
        } else if(username.length <= 8){
            console.log("Username is too short")
        } else if(password.length <= 8){
            console.log("password is too short")
        }else {
            console.log("Credentials too short")
        }


    } else {
        console.log("One of the arguments is not a string")
    }
}


login("janesmith", "jane1234")



//function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if(windSpeed < 30) {
        return 'Not a typhoon yet.';
    }
    else if(windSpeed <= 61) {
        return 'Tropical depression detected.';
    }
    else if(windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropical storm detected.';
    }
    else if(windSpeed >= 89 && windSpeed <= 117) {
        return 'Severe tropical storm detected.'
    }
    else {
        return 'Typhoon detected.';
    }
}

message = determineTyphoonIntensity(68);
console.log(message);
//console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

if (message == 'Tropical storm detected.'){
    console.warn(message);
}




//Truthy and Falsy
//truthy
if(true) {
    console.log('Truthy')
}

if(1) {
    console.log('True')
}

if([]) {
    console.log('Truthy')
}


//falsy
//-0, "", null, NaN
if(false){
    console.log('Falsy')
}


if(0) {
    console.log('False')
}

if(undefined) {
    console.log('Falsy')
}

//Ternary Operator

/*
Syntax;
    (expression/condition) ? ifTrue : ifFalse;
three operatnd of ternary operator:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is false


Ternary operators were created so that we can have an if-else statement in one line
*/

//Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of ternary operator ${ternaryResult}`);

// if(1<18) {
//  console.log(true) 
// } else {
//  console.log(false)
// }


let price = 50000;

price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000")



let villain = "Harvey Dent";

villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villainous yet.")

//Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable.


//miniactivity

let DayToday = prompt('Type a day:')

function wearToday(DayToday){
    if(typeof DayToday === 'string'){

        if(DayToday.toLowerCase() == 'monday'){
            alert(`Today is ${DayToday}, Wear Black`);
        } else if(DayToday.toLowerCase() == 'tuesday'){
            alert(`Today is ${DayToday}, Wear Green`);
        } else if(DayToday.toLowerCase() == 'wednesday'){
            alert(`Today is ${DayToday}, Wear Yellow`);
        } else if(DayToday.toLowerCase() == 'thursday'){
            alert(`Today is ${DayToday}, Wear Red`);
        } else if(DayToday.toLowerCase() == 'friday'){
            alert(`Today is ${DayToday}, Wear Violet`);
        } else if(DayToday.toLowerCase() == 'saturday'){
            alert(`Today is ${DayToday}, Wear Blue`);
        } else if(DayToday.toLowerCase() == 'sunday'){
            alert(`Today is ${DayToday}, Wear White`);
        } else {
            alert(`Invalid Input. Enter a valid day of the week`);
        }
        

    } 
        else {
        alert('Invalid Input. Please put a string')
    
    }
    }
    

wearToday(DayToday);

